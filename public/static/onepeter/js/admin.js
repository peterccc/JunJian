function ajaxAlert(code,msg,wait,url) {
    var c;
    if(code) {
        c = 'alert-success';
        if(wait){
            setTimeout(function(){
                if(window.location.href == url) {
                    location.reload(true);
                }else {
                    location.href=url;
                }

            },wait*1000);
        }
    }else {
        c = 'alert-danger';
    }
    var win = $('<div class="alert '+c+'" style="z-index:999;position: fixed;top: 90%;left: 50%;min-width: 200px;max-width: 200px;width: 200px;">'+msg+'<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>').appendTo('body');
    if(wait) {
        setTimeout(function () {
            win.remove();
        },wait * 1000);
    }
}

$(function(){
    (function(){
        $('.ajax-post-ids').on('click',function () {
            var url = $(this).attr('url');
            var ids = [];
            $('[name="ids[]"]').each(function (index,elem) {
                if($(this).prop('checked')) {
                    ids.push($(this).val());
                }

            });
            $.post(url,{'ids':ids},function (data,status) {
                ajaxAlert(data.code,data.msg,data.wait,data.url);
            });
            return false;
        });
    })();


    /**
     * 属性含有btn-a者，点击会跳转到url指定url，如果target=_blank则新开一页跳转
     */
    (function(){
        $('.btn-a').on("click",function () {
            var url = $(this).attr('url');
            if(!url) {
                url = $(this).attr('href');
            }
            var target = $(this).attr('target');
            switch (target){
                default:
                case '_self':
                    window.location.href=url;
                    break;
                case '_blank':
                    window.open(url);
                    break;
            }
        });
    })();


    /**
     * ajax post请求 url 属性的url
     */
    (function(){
        $('.ajax-post').on('click',function(){
            var url = $(this).attr('href');
            if(!url) {
                url = $(this).attr('url');
            }
            $.post(url,{},function(data,status){
                ajaxAlert(data.code,data.msg,data.wait,data.url);
            });
            return false;
        });
    })();


    /**
     * 属性含有ajax-post-submit者，点击后将会发送ajax post 到url属性指定位置，如果没有url则发送到form的action
     */
    (function(){
        function getFormData(form) {
            var values = {};
            var input = form.find('[name]');
            input.each(function(index,ele){
                var value = $(this).val();
                var name = $(this).prop('name');

//                if(value && name) {   // 因为上传图片如果全取消，value为空，所以不能判断value不为空才进行提交
                if(name) {
                    if(name.indexOf("[]") > 0) {
                        name = name.replace('[]','');
                        var arr = eval('values.' + name);
                        if(arr) {
                            arr.push(value);
                        }else {
                            arr = [value];
                        }
                        eval("values." + name + "=arr");
                    }
                    eval("values." + name + "=value");
                }
            });
            return values;
        }
        $('.ajax-post-submit').on("click",function () {
            var url = $(this).attr('url');
            var form = $(this).parents("[target-form=true]");
            if(!url) {
                url = form.attr('action');
                if(!url){
                    url = '/';
                }
            }
            var data = getFormData(form);
            $.post(url,data,function (data,status) {
                ajaxAlert(data.code,data.msg,data.wait,data.url);
            });
            return false;
        })
    })();


});


