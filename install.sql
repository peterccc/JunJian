CREATE TABLE IF NOT EXISTS `onepeter_cms_img`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `type` int NOT NULL , /*1:local 本地服务器中的, 2:external 外面其他服务器中的*/
  `path` VARCHAR (255) NOT NULL
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


CREATE TABLE IF NOT EXISTS `onepeter_cms_file`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `type` int NOT NULL , /*1:local 本地服务器中的, 2:external 外面其他服务器中的*/
  `path` VARCHAR (255) NOT NULL
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;



CREATE TABLE IF NOT EXISTS `onepeter_cms_admin`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `username` VARCHAR (32) NOT NULL ,
  `password` CHAR (32) NOT NULL
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `onepeter_cms_admin` VALUES (1,'admin',md5('123456'));



CREATE TABLE IF NOT EXISTS `onepeter_cms_module`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `name` VARCHAR (32) NOT NULL ,
  `config` TEXT NOT NULL
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;
INSERT INTO `onepeter_cms_module` VALUES (1,'test','{}');

CREATE TABLE IF NOT EXISTS `onepeter_config` (
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `name` VARCHAR (100) NOT NULL UNIQUE ,
  `value` TEXT,
  `option` TEXT /*用json来配置其他选项配置*/
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;


/*weipeter*/
/**
保存着微信公众号
 */
CREATE TABLE IF NOT EXISTS `onepeter_mp`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `alias` VARCHAR(50) NOT NULL ,  /*微信公众号的昵称，用来说明公众号的作用*/
  `debug` TINYINT UNSIGNED NOT NULL DEFAULT 1,  /*1表示true，0表示false*/
  `originid` VARCHAR (50) NOT NULL ,
  `appid` VARCHAR (50) NOT NULL ,
  `appsecret` VARCHAR (50) NOT NULL ,
  `token` VARCHAR (50) NOT NULL ,
  `interface_token` VARCHAR (50) NOT NULL ,
  `create_time` INT UNSIGNED NOT NULL
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;

/**
自动回复
 */
CREATE TABLE IF NOT EXISTS `onepeter_mp_reply`(
  `id` INT unsigned PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `keyword` VARCHAR(50) UNIQUE ,
  `type` TINYINT NOT NULL , /*1=文本内容,2=让模块来处理*/
  `content` TEXT NOT NULL /*type=1 则这里是文本内容，type=2 则这里是模块名*/
)ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;