<?php
namespace app\index\validate;

use think\Validate;

class IndexUserInfo extends Validate {
    protected $name = 'index_user_info';

    // 验证规则
    protected $rule = [
        ['height|身高' , 'require|integer', '请填写身高|身高不能非数字'],
        ['sex|性别'    , 'require|integer', '请填写性别|错误'],
        ['weight|重量' , 'require|integer', '请填写重量|重量不能非数字'],
        ['waist|腰围' , 'require|integer', '请填写腰围|腰围不能非数字'],
        ['bust|胸围' , 'require|integer', '请填写胸围|胸围不能非数字'],
        ['interest|兴趣' , 'require|integer', '请填写兴趣|错误'],
        ['character|性格' , 'require|integer', '请填写性格|错误']
    ];
}