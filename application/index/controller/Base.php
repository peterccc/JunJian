<?php
namespace app\index\controller;

use app\admin\controller\MpOauth;
use app\index\model\UserInfo;

class Base extends MpOauth {
    protected function _initialize()
    {
        parent::_initialize();

        $userInfo = session("index.userInfo");
        if(!$userInfo) {
            $userInfo = UserInfo::get(['openid'=>$this->getInfo()['id']]);
            if($userInfo === null) {
                // 记录这次访问的是什么网站
                $url = "http://" . $_SERVER['HTTP_HOST'] . ':' . $_SERVER['SERVER_PORT'] . $_SERVER['PHP_SELF'];
                session("index.accessBeforeRegisterUrl", $url);
                // 还没注册用户
                $this->redirect("Register/index");
                exit();
            }else {
                // 已经注册用户，现在将他写到session中
                session("index.userInfo", $userInfo);
            }
        }

    }


    /**
     * 全局授权
     * 返回数组，返回需要的Scopes，如果这里返回的不是SCOPES_NOT_OAUTH，则所有方法都会被网页授权，如果返回SCOPES_NOT_OAUTH，则所有网页都不会被网页授权
     * @return mixed
     */
    protected function getScopes()
    {
        return [MpOauth::SCOPES_SNSAPI_USERINFO];
    }


}