<?php
namespace app\index\controller;
use app\admin\controller\MpOauth;
use app\index\model\UserInfo;
use think\Controller;
use think\Request;

class Register extends MpOauth {
    public function index(Request $request) {
        if($request->isPost()) {
            $userInfo = new UserInfo();


            if($userInfo->register($this->getMpId(),$this->getOpenId(),input('post.'))) {
                $url = session("index.accessBeforeRegisterUrl");
                if($url === null) {
                    $url = "Index/Index/index";
                }
                $this->success("注册成功", $url);

            }else {
                $this->error($userInfo->getError());
            }

        }else {
            return $this->fetch();
        }

    }

    /**
     * 全局授权
     * 返回数组，返回需要的Scopes，如果这里返回的不是SCOPES_NOT_OAUTH，则所有方法都会被网页授权，如果返回SCOPES_NOT_OAUTH，则所有网页都不会被网页授权
     * @return mixed
     */
    protected function getScopes()
    {
        return [MpOauth::SCOPES_SNSAPI_USERINFO];
    }
}