<?php
namespace app\index\model;
use think\Model;

class UserInfo extends Model {
    protected $name = 'index_user_info';

    public function register($mpid,$openid,$data){
        $data = array_merge($data, [
            'mpid' => $mpid,
            'openid' => $openid
        ]);
        return $this->allowField(true)->validate(true)->save($data);
    }
}