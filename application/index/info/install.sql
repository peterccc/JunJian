/**
身高，性别，体重，腰围，胸围，兴趣爱好，性格等
 */
CREATE TABLE IF NOT EXISTS `onepeter_index_user_info`(
  `id` INT UNSIGNED PRIMARY KEY auto_increment UNIQUE NOT NULL ,
  `mpid` INT UNSIGNED NOT NULL ,
  `openid` VARCHAR (50) NOT NULL ,
  `height` INT , /*身高，单位：cm*/
  `sex` TINYINT ,   /*性别: 0 男， 1 女*/
  `weight` INT , /*重量，单位：kg*/
  `waist` INT , /*腰围，单位：cm*/
  `bust` INT , /*胸围，单位：cm*/
  `interest` INT ,
  `character` INT
)ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;