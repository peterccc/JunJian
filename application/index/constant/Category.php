<?php
namespace app\index\constant;
/*
兴趣爱好：
1、运动类：如篮球、羽毛球等
2、文艺类：如读书、看电影、听音乐等
性格：
外向型：积极乐观、活泼、善谈等
内向型：腼腆、害羞等
 */
class Category {
    const INTEREST = [
        'basketball' => [
            'id' => 1,
            'keyName' => 'basketball',
            'cnName' => '篮球'
        ],
        'badminton' => [
            'id' => 2,
            'keyName' => 'badminton',
            'cnName' => '羽毛球'
        ]
    ];

    const CHARACTER = [
        'hopeful' => [
            'id' => 1,
            'keyName' => 'hopeful',
            'cnName' => '积极乐观'
        ],
        'lively' => [
            'id' => 2,
            'keyName' => 'lively',
            'cnName' => '活泼'
        ],
    ];
}