<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 通过图片id获取图片数据模型
 * @param $id
 * @return static
 */
function get_img($id) {
    $img = new \app\admin\model\Img();
    return $img->getImg($id);
}
function get_file($id) {
    $file = new \app\admin\model\File();
    return $file->getFile($id);
}

/**
 * 通过图片id获取图片url
 * @param $id
 * @return string
 */
function get_img_path($id) {
    $img = get_img($id);
    if($img) {
        if($img->type == \app\admin\Model\Img::TYPE_LOCAL) {
            return ROOT_URL . $img->path;
        }
    }
    return false;
}
function get_file_path($id) {
    $file = get_file($id);
    if($file) {
        if($file->type == \app\admin\model\File::TYPE_LOCAL) {
            return ROOT_URL . $file->path;
        }
    }
    return false;
}

/**
 * 图片数组转字符串，在ConfigBuilder接收到上传图片ids，保存ids的时候使用
 * @param $arr
 * @return string
 */
function img_array_to_string($arr) {
    return implode(",",$arr);
}
function file_array_to_string($arr) {
    return implode(",",$arr);
}

function string_to_img_array($str) {
    if($str) {
        return explode(',',$str);
    }else {
        return [];
    }

}
function string_to_file_array($str) {
    if($str) {
        return explode(",",$str);
    }else {
        return [];
    }

}

/**
 * 获取当前被选择的公众号(指的是后台被选中)
 * @return mixed
 */
function get_selected_mp() {
    return \app\admin\model\Mp::getSelectedMp();
}
function get_bind_mp() {
    $id = \app\common\model\Config::getValue('WEIPETER_MP_BIND');
    if($id) {
        $mp = \app\admin\model\Mp::get($id);
        if($mp) {
            return $mp->toArray();
        }
    }
    return null;
}