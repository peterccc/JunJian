<?php
namespace app\admin\core;
use app\admin\model\Module as ModuleModel;

/**
 * Class Install
 * @package app\admin\core
 * 用来安装模块，获取模块信息之类的操作
 */
class Module {
    const FILTER_MODULE = [
        'admin',
        'common',
        'extra',
    ];
    const FILE_MODULE_CONFIG = 'info/info.php';     // 相对模块目录，config文件的路径
    const FILE_MODULE_INSTALL = 'info/install.sql';
    const FILE_MODULE_UNINSTALL = 'info/uninstall.sql';
    const FILE_MODULE_CLEAR_DATA = 'info/cleardata.sql';
    private $moduleDb;

    public function __construct() {
        $this->moduleDb = new ModuleModel();
    }
    public function getInstalledModuleList() {
        return $this->moduleDb->getInstalledList();
    }

    public function isInstall($moduleName) {
        foreach ($this->getInstalledModuleList() as $module) {
            if($module['name'] == $moduleName) {
                return true;
            }
        }
        return false;
    }
    public function install($moduleName) {
        $json = json_encode($this->loadConfigFile($moduleName));
        $sql = $this->loadInstallSql($moduleName);
        return $this->moduleDb->install($moduleName,$json,$sql);
    }
    public function uninstall($moduleName) {
        $sql = $this->loadUninstallSql($moduleName);
        return $this->moduleDb->uninstall($moduleName,$sql);

    }
    public function clearData($moduleName) {
        $sql = $this->loadClearDataSql($moduleName);
            return $this->moduleDb->clearData($sql);
    }
    public function getModuleNameList() {
        return $this->readdir(APP_PATH,$this::FILTER_MODULE);
    }
    public function getModuleList() {
        $modules = $this->getModuleNameList();
        $data = [];
        foreach ($modules as $module) {
            if($this->isInstall($module)) {
                $status = true;
            }else {
                $status = false;
            }
            $data[] = ['name' => $module, 'status' => $status];
        }
        return $data;
    }

    /**
     * 读取配置文件
     * @param $moduleName
     * @return bool|mixed
     */
    public function loadConfigFile($moduleName) {
        if(!($configFilePath = realpath(APP_PATH . DS . $moduleName . DS . $this::FILE_MODULE_CONFIG))) {
            return false;
        }
        return require($configFilePath);
    }

    /**
     * 读取数据库的模块配置
     * @param $moduleName
     * @return mixed
     */
    public function getConfig($moduleName) {
        return json_decode($this->moduleDb->getConfig($moduleName),true);
    }

    private function loadInstallSql($moduleName){
        return $this->loadFileContent($moduleName,$this::FILE_MODULE_INSTALL);
    }
    private function loadUninstallSql($moduleName) {
        return $this->loadFileContent($moduleName,$this::FILE_MODULE_UNINSTALL);
    }
    private function loadClearDataSql($moduleName) {
        return $this->loadFileContent($moduleName,$this::FILE_MODULE_CLEAR_DATA);
    }
    private function loadFileContent($moduleName,$relativePath) {
        $file = $this->getPath($moduleName,$relativePath);
        if(file_exists($file))
            return file_get_contents($file);
        else
            return false;
    }
    private function getPath($moduleName,$relativePath) {
        return APP_PATH . DS . $moduleName . DS . $relativePath;
    }
    /**
     * 注意，返回的数组，只有目录，没有文件
     * @param $dir
     * @param array $filter
     * @return array
     */
    private function readdir($dir,$filter = []) {
        $filter = array_merge($filter,['.','..']);
        $cur = getcwd();
        chdir($dir);
        $handle = opendir($dir);
        $ret = [];
        while (false !== ($file = readdir($handle))) {
            if(is_dir(($file)) && !in_array($file,$filter))
                $ret[] = $file;
        }
        closedir($handle);
        chdir($cur);
        return $ret;
    }


}