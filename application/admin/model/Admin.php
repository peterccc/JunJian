<?php
namespace app\admin\model;
use think\Config;
use think\Model;
class Admin extends Model {
    protected $name = 'cms_admin';

    public function login($username,$password) {
        $map['username'] = $username;
        $map['password'] = md5(Config::get("salt") . $password);
        $admin = Admin::get($map);
        if($admin) {
            return $admin;
        }else {
            return false;
        }
    }
    public function register($username,$password) {
        $data['username'] = $username;
        $data['password'] = md5(Config::get("salt") .$password);
        if($result = Admin::create($data)){
            return true;
        }else {
            return false;
        }
    }
}