<?php
namespace app\admin\model;
use think\Db;
use think\Model;
class Module extends Model {
    protected $name = 'cms_module';
    public function getInstalledList() {
        return $this->field('id,name')->select();
    }
    public function getConfig($name) {
        $map['name'] = $name;
        $data = $this->where($map)->field('config')->find();
        return $data['config'];
    }
    public function install($name,$config,$installSql) {
        $data['name'] = $name;
        $data['config'] = $config;
        if($this->save($data)) {
            $this->execStrSql($installSql);
            return true;
        }else {
            return false;
        }
    }
    public function uninstall($name,$uninstallSql) {
        $map['name'] = $name;
        if($this->where($map)->delete()){
            $this->execStrSql($uninstallSql);
            return true;
        }else {
            return false;
        }
    }
    public function clearData($clearSql) {
        $this->execStrSql($clearSql);
    }
    private function execStrSql($str) {
        if($str) {
            $arr = explode(";",$str);

            foreach ($arr as $val) {
                $val = trim($val);
                if($val == '') {
                    continue;
                }
                Db::execute($val . ';');
            }
        }

    }
}