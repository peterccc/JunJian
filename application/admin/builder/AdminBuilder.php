<?php

namespace app\admin\builder;

use app\admin\controller\Admin;
use think\Controller;
use think\Request;

/**
 * Class AdminBuilder
 * @package app\admin\builder
 * 不需要继承Admin，因为tp5中，assign分配的变量是全局的，一个控制器assign一下，其他任何控制器都能访问
 */
class AdminBuilder extends Controller
{
    /**
     * @var Request $_request
     */
    protected $_request;

    protected function _initialize()
    {
        parent::_initialize();
        $this->_request = Request::instance();
    }


    /**
     * @param $attr array 例如: ['class'=>’btn btn-default‘,'type'=>'button']
     * @return array|string 例如:'class="btn btn-default" type="button"'
     */
    protected function compileHtmlAttr($attr) {
        $result = array();
        foreach($attr as $key=>$value) {
            $value = htmlspecialchars($value);
            $result[] = "$key=\"$value\"";
        }
        $result = implode(' ', $result);
        return $result;
    }



    /*******/
    protected $_title;
    protected $_suggest;


    public function fetch($template = '', $vars = [], $replace = [], $config = [])
    {
        $this->assign("_title",$this->_title);
        $this->assign("_suggest",$this->_suggest);
        return parent::fetch("admin@admin/{$template}");
    }

    public function title($title) {
        $this->_title = $title;
        return $this;
    }

    public function suggest($suggest) {
        $this->_suggest = $suggest;
        return $this;
    }
}
