<?php
namespace app\admin\controller;
use app\admin\model\Admin;
use app\common\model\UserStatus;
use think\Controller;
use think\Request;
use think\Url;
class Access extends Controller{
    public function login(Request $request,$username='',$password='',$captcha='') {
        if($request->isGet()) {
            // 登录页面
            return $this->fetch();
        }else {
            // 登录处理
            if(!captcha_check($captcha)){
                $this->error("验证码错误");
            }
            $admin = new Admin();
            if(!($user = $admin->login($username,$password))) {
                $this->error("用户名或密码错误");
            }
            $userStatus = new UserStatus();
            $userStatus->login($user->id,$user->username,true);
            $this->redirect(Url::build('Index/index'));
        }
    }
    public function logout() {
        $userStatus = new UserStatus();
        $userStatus->logout();
        $this->redirect(Url::build('login'));
    }
}