<?php

namespace app\admin\controller;

use app\admin\core\Module;
use app\common\model\UserStatus;
use think\Controller;

abstract class Admin extends Controller {
    protected function _initialize()
    {
        parent::_initialize();
        // 验证是否登录
        $userStatus = new UserStatus();
        if(!$userStatus->isAdmin()){
            $this->error("请先登录",'admin/Access/login');
        }

        // 生成页面
        $install = new Module();


        // 生成导航条
        /*$_navToolBarConfig = [
            [
                'title' => '首页',
                'active' => true,
                'href' => '#'
            ],
            [
                'title' => '用户',
                'href' => '#',
            ],
        ];*/
        if($config = $install->getConfig("admin")){
            $_navToolBarConfig = $config['nav_tool_bar_config'];
            $this->assign('_navToolBarConfig',$_navToolBarConfig);
        }

        // 生成侧边导航栏
        /*$_navSideBarConfig = [
            [
                'title' => '标题1',
                'active' => true,
                'buttonList' => [
                    [
                        'title' => '按钮1',
                        'active' => true,
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
            [
                'title' => '标题2',
                'buttonList' => [
                    [
                        'title' => '按钮1',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮2',
                        'href' => '#',
                    ],
                    [
                        'title' => '按钮3',
                        'href' => '#',
                    ],
                ]
            ],
        ];*/
        $list = $install->getInstalledModuleList();
        $_navSideBarConfig = [];
        foreach ($list as $module) {
            $config = $install->getConfig($module['name']);
            $_navSideBarConfig = array_merge($_navSideBarConfig,$config['nav_side_bar_config']);
        }
        $this->assign('_navSideBarConfig',$_navSideBarConfig);

        // 当前公众号
        $curMp = get_selected_mp();
        if($curMp)
            $curMpAlias = $curMp['alias'];
        else
            $curMpAlias = '无';
        $this->assign('curMpAlias',$curMpAlias);

        // 绑定公众号
        $bindMp = get_bind_mp();
        if($bindMp)
            $bindMpAlias = $bindMp['alias'];
        else
            $bindMpAlias = '无';
        $this->assign('bindMpAlias',$bindMpAlias);




    }

}