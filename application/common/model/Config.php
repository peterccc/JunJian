<?php
namespace app\common\model;
use think\Model;
class Config extends Model {
    public static function setConfig($name,$value,$option = []) {
        $option = json_encode($option);
        // 先判断是否有这个选项
//            $data = new Config();
//            $data->name = $name;
//        $data->value = $value;
//        $data->option = $option;
//
        //$data->save();
        if(!($data = Config::get(['name'=>$name]))) {
            $data = [];
            $data['name'] = $name;
            $data['value'] = $value;
            $data['option'] =$option;
            Config::create($data);
        }else {
            $data->value = $value;
            $data->option = $option;
            $data->save();
        }

    }
    public static function getAll() {
        return Config::all();
    }
    public static function getValue($name) {
        $config = Config::get(['name'=>$name]);
        if($config) {
            return $config['value'];
        }else {
            return null;
        }

    }
    public static function getOption($name) {
        $config = Config::get(['name'=>$name]);
        if($config) {
            return json_decode($config['option'],true);
        }else {
            return null;
        }
    }
}