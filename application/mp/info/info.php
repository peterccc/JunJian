<?php
    return [
        'nav_side_bar_config' => [
            [
                'title' => '公众号基本功能',
                'buttonList' => [
                    [
                        'title' => '用户列表',
                        'href' => \think\Url::build('mp/admin/userList'),
                    ],
                    [
                        'title' => '留言列表',
                        'href' => \think\Url::build('mp/admin/msgList'),
                    ],
                    [
                        'title' => '场景二维码',
                        'href' => \think\Url::build('mp/admin/qrcodeList'),
                    ],
                    [
                        'title' => '扫码统计',
                        'href' => \think\Url::build('mp/admin/qrcodeStatisticsList'),
                    ]
                ]
            ],
        ]
    ];